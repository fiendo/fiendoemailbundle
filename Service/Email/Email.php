<?php
namespace Fiendo\EmailBundle\Service\Email;

class Email
{
    protected $client;

    public function __construct(ClientAdapter $client)
    {
        $this->client = $client;
    }

    public function send(
        $from,
        $to,
        $subject,
        $htmlBody,
        $textBody,
        $attachments = null
    ) {
        $this->client->sendEmail(
            $from,
            $to,
            $subject,
            $htmlBody,
            $textBody,
            $attachments
        );
    }
}