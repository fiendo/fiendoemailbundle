<?php
namespace Fiendo\EmailBundle\Service\Email;

interface ClientAdapter
{
    /**
     * Send an email.
     *
     * @param  string $from The sender of the email. (Your account must have an associated Sender Signature for the address used.)
     * @param  string $to  The recipient of the email.
     * @param  string $subject  The subject of the email.
     * @param  string $htmlBody  The HTML content of the message, optional if Text Body is specified.
     * @param  string $textBody  The text content of the message, optional if HTML Body is specified.
     * @param  array $attachments  An array of string with path to the files
     */
    public function sendEmail(
        $from,
        $to,
        $subject,
        $htmlBody,
        $textBody,
        $attachments
    );
}