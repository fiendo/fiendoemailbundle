<?php
namespace Fiendo\EmailBundle\Service\Email;

class PostmarkAdapter implements ClientAdapter
{
    protected $postmarkClient;

    public function __construct(PostmarkClient $postmarkClient)
    {
        $this->postmarkClient = $postmarkClient;
    }

    public function sendEmail(
        $from,
        $to,
        $subject,
        $htmlBody,
        $textBody,
        $attachments
    ) {
        $this->postmarkClient->sendEmail(
            $from,
            $to,
            $subject,
            $htmlBody,
            $textBody,
            null,
            null,
            null,
            null,
            null,
            null,
            $attachments
        );
    }
}