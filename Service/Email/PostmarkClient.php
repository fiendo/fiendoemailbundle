<?php
namespace Fiendo\EmailBundle\Service\Email;

use Postmark\PostmarkClient as Client;

class PostmarkClient
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function sendEmail(
        $from,
        $to,
        $subject,
        $htmlBody,
        $textBody,
        $attachments
    ) {
        $client = new Client($this->apiKey);

        $client->sendEmail(
            $from,
            $to,
            $subject,
            $htmlBody,
            $textBody,
            null,
            null,
            null,
            null,
            null,
            null,
            $attachments
        );
    }
}